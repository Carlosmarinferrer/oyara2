import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '../receptor-visor-cuentas/receptor-visor-cuentas.js'

 /* import '../receptor-evento/receptor-evento.js' -->
/**
 * @customElement
 * @polymer
 */
class ReceptorVisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <h2> soy el receptor visor usuario</h2>
      <h3> este es el id [[id]]</h3>
      <!--      <input type="text" value="{{course::input}}" /> -->

      <h4> el nombre es [[first_name]]</h4>
      <h5> el apelliedo es [[last_name]]</h5>
      <receptor-visor-cuentas id="usuario"></receptor-visor-cuentas>

      <iron-ajax

       id="getUser"
       url="http://localhost:3000/apitechu/v2/users/{{id}}"
       handle-as="json"

         content-type="application/json"
         on-response="manageAJAXResponse"
         on-error="showError"
      >
      </iron-ajax>


    `;
  }
  static get properties(){
    return {
      id: {
        type: Number, observer: "_idusuarioChanged"
      }
  //    , first_name: {
  //      type: String
  //    }, second_name: {
  //      type: String
  //    }

    }
  };

  _idusuarioChanged(newValue, oldValue){
      console.log("idusuario value has changed")
      console.log("old value was " + oldValue)
      console.log("new value is " + newValue)



      this.$.getUser.generateRequest();

    }
    manageAJAXResponse(data){
      console.log("llegaron los resultados")
      console.log(data.detail.response.first_name)
      this.first_name = data.detail.response.first_name
      this.last_name = data.detail.response.last_name
      this.$.usuario.id = data.detail.response.id
    }
    showError(error) {
      console.log("hubo un error")
      console.log(error)
      console.log(error.detail.request.xhr.response)
      this.isLogged = false;
    }







}

window.customElements.define('receptor-visor-usuario', ReceptorVisorUsuario);
