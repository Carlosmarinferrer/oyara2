import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
/**
 * @customElement
 * @polymer
 */
class ReceptorVisorCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
  <!--    <input type="id" placeholder="id"  value="{{id::input}}"/> -->
      <br/>

      <button on-click="buscar">Buscar</button>



  <!--    <h2>La cuenta es [[iban]]y el es[[balance]]</h2> -->
  <!--    <h2>y mi id es  [[id]]</h2>-->
      <h3>Sus cuentas </h3>
         <dom-repeat items="{{cuentas}}">
              <template>
                 <h4>IBAN - {{item.iban}}</h4>
                 <h4>balance - {{item.balance}}</h4>
              </template>
         </dom-repeat>

      <iron-ajax
         id="doBuscar"
         url="http://localhost:3000/apitechu/v2/cuentas/{{id}}"
         handle-as="json"
         content-type="application/json"

         on-response="showData"
         on-error="showError"
      >
      </iron-ajax>



    `;
  }
  static get properties(){
    return {
      iban: {
        type: String
      },
      id: {
        type: Number
      },
      balance:{
          type: Number
      }
    }
  };

  showData(data){
    console.log("showData")
    console.log(data.detail.response[0].iban)
    this.cuentas = data.detail.response;
//    this.iban = data.detail.response[0].iban;
//    this.id = data.detail.response[0].id;
//    this.balance = data.detail.response[0].balance;
  }

  buscar() {
    console.log("El usuario ha puosado el boton de cuentas")
    
    this.$.doBuscar.generateRequest();
//    console.log(loginData)
  }
  manageAJAXResponse(data){
    console.log("llegaron los resultados")
    console.log(data.detail.response)
    this.isLogged = true;
  }
  showError(error) {
    console.log("hubo un error")
    console.log(error)
    console.log(error.detail.request.xhr.response)
    this.isLogged = false;
  }
}

window.customElements.define('receptor-visor-cuentas', ReceptorVisorCuentas);
